class Dealer:

    def __init__(self):
        self.cards = {
            'Q': 10,
            '6': 6,
            '7': 7
        }

    def can_hit(self, cards):
        sum = 0
        for card in cards:
            sum += self.cards[card]

        print("sum: {}".format(sum))
        if sum < 17:
            return True
        else:
            return False

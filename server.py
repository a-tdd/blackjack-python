import falcon
from dealercontroller import DealerController

app = falcon.API()
app.add_route('/api/dealer/action', DealerController())

## How to rum server

```shell script
source venv/bin/activate
pip install requirements.txt
gunicorn server:app
```

## How to run tests
```shell script
behave
```

### Todo
* Исправьте падающий тест
* Сервер должен принмать карты в любом регистре
* Напишите примеры по правилу 1 и автоматизируйте их (feature-файлы, клей и код):  Дилер сдает карты до тех пор пока сумма не будет 17 или больше
* Напишите тест на неправильную карту, например: "Joker"
* Напишите пример на ограничение по количеству карт (max=5) 
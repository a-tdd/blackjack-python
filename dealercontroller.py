import falcon
import json
from domain.dealer import Dealer


class DealerController:

    def __init__(self):
        self.dealer = Dealer()

    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        cards = req.get_param('hand')
        print("cards: {}".format(cards))
        result = {
            'can hit': self.dealer.can_hit(cards.split(','))
        }
        # resp.co
        resp.body = json.dumps(result)

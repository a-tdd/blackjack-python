from behave import given, when, then
import requests


def can_hit(action):
    if action == "hits":
        return True
    else:
        return False


@given('has "{card}" on his hands')
def dealer_has_card(context, card):
    context.cards = [card]


@when('he hit "{card}" from the deck')
def dealer_hit_card(context, card):
    context.cards.append(card)


@then('he "{action}"')
def dealer_take_action(context, action):
    hand = ",".join(context.cards)
    url = 'http://localhost:8000/api/dealer/action?hand=' + hand
    result = requests.get(url)
    assert result.json()["can hit"] == can_hit(action)

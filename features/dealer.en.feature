Feature: Dealer takes cards

  Scenario: dealer takes 17
    Given has "Q" on his hands
    When he hit "7" from the deck
    Then he "stands"

  Scenario: dealer takes 16
    Given has "Q" on his hands
    When he hit "6" from the deck
    Then he "hits"

  Scenario: dealer gets blackjack
    Given has "Q" on his hands
    When he hit "A" from the deck
    Then he "hits"
